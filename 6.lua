function r(T, R)
	local sqrt_delta = math.sqrt(T^2 - 4*R)
	return 0.5 * (T - sqrt_delta), 0.5 * (T + sqrt_delta)
end

File = io.lines(arg[1])

local times_str = File()
local times = {}
for t in string.gmatch(times_str, "%d+") do
	times[#times+1] = tonumber(t)
end

local distances_str = File()
local distances = {}
for d in string.gmatch(distances_str, "%d+") do
	distances[#distances+1] = tonumber(d)
end

local result1 = 1
for i = 1, #times do
	local r1, r2 = r(times[i], distances[i] + 0.1)
	result1 = result1 * (math.floor(r2) - math.floor(r1))
end
print("Result1", result1)

local r1, r2 = r(
	tonumber(times_str:gsub(" ", ""):match("%d+")),
	tonumber(distances_str:gsub(" ", ""):match("%d+")) + 0.1
)
print("Result2", math.floor(r2) - math.floor(r1))
