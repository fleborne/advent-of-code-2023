if #arg < 1 then
	print("filename needed")
	return
end

Sum = 0
for l in io.lines(arg[1]) do
	local i = 0
	local number = 0
	repeat
		i = i + 1
		number = tonumber(string.sub(l, i, i), 10)
	until number ~= nil
	Sum = Sum + 10 * number

	i = #l + 1
	repeat
		i = i - 1
		number = tonumber(string.sub(l, i, i), 10)
	until number ~= nil
	Sum = Sum + number
end

print(Sum)
