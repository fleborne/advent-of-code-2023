if #arg < 1 then
	print("Filename needed")
	return
end

Result1 = 0
Result2 = 0
Id = 0

NumRed = 12
NumGreen = 13
NumBlue = 14

MinRed = 0
MinGreen = 0
MinBlue = 0

for l in io.lines(arg[1]) do
	MinRed = 0
	MinGreen = 0
	MinBlue = 0
	Id = Id + 1
	local valid = true
	for draw in string.gmatch(l, "%d+ %a+") do
		local str_n, color = string.match(draw, "(%d+) (%a+)")
		local number = tonumber(str_n)

		if (color == "red" and number > NumRed) or (color == "green" and number > NumGreen) or (color == "blue" and number > NumBlue) then
			valid = false
		end

		if color == "red" and number > MinRed then
			MinRed = number
		end

		if color == "green" and number > MinGreen then
			MinGreen = number
		end

		if color == "blue" and number > MinBlue then
			MinBlue = number
		end
	end

	if valid then
		Result1 = Result1 + Id
	end

	local power = MinRed * MinGreen * MinBlue
	Result2 = Result2 + power
end

print("Result part 1", Result1)
print("Result part 2", Result2)
