if #arg < 1 then
	print("Filename needed")
	return
end

function CheckAdjChar(line, number, linenum, colnum)
	local adj_char = string.sub(line, colnum, colnum)
	if adj_char ~= "." then
		if adj_char == "*" then
			GearRatio(number, linenum, colnum)
		end
		return true
	end
	return false
end

function CheckAdjLine(linenum, number, ext_first_index, ext_last_index)
	local str = string.sub(
		Lines[linenum],
		ext_first_index,
		ext_last_index
	)
	local match_start_index = string.find(str, "[^%.]")
	if match_start_index then
		if string.sub(str, match_start_index, match_start_index) == "*" then
			GearRatio(
				number,
				linenum,
				match_start_index + ext_first_index - 1
			)
		end
		return true
	end
	return false
end

function GearRatio(number, linenum, colnum)
	for k = 1, #Gears do
		local gear = Gears[k]
		if gear.linenum == linenum and gear.colnum == colnum then
			Result2 = Result2 + number * gear.num
		end
	end
	table.insert(Gears, {num = number, linenum = linenum, colnum = colnum})
end

Result1 = 0
Result2 = 0
Lines = {}
Gears = {}

for l in io.lines(arg[1]) do
	Lines[#Lines+1] = l
end

for linenum = 1, #Lines do
	local line = Lines[linenum]
	local colnum = 1
	while colnum < #line + 1 do
		local toAdd = false
		local first_index, last_index = string.find(line, "%d+", colnum)
		if not first_index then
			break
		end

		local number = tonumber(string.sub(line, first_index, last_index))
		local ext_first_index = first_index
		local ext_last_index = last_index
		colnum = last_index + 1

		if first_index > 1 then
			ext_first_index = first_index - 1
			toAdd = CheckAdjChar(line, number, linenum, ext_first_index)
		end

		if not toAdd and last_index < #line then
			ext_last_index = last_index + 1
			toAdd = CheckAdjChar(line, number, linenum, ext_last_index)
		end

		if not toAdd and linenum > 1 then
			toAdd = CheckAdjLine(linenum - 1, number, ext_first_index, ext_last_index)
		end

		if not toAdd and linenum < #Lines then
			toAdd = CheckAdjLine(linenum + 1, number, ext_first_index, ext_last_index)
		end

		if toAdd then
			Result1 = Result1 + number
		end

	end
end

print("Result1", Result1)
print("Result2", Result2)
