File = io.lines(arg[1])
Instr = File()
Map = {}

_ = File()

local line = File()
while line ~= nil do
	local key, left, right = line:match("(%w+) = %((%w+), (%w+)%)")
	Map[key] = {L = left, R = right, visits = 0}
	line = File()
end

function GetSteps(current)
	local step = 0
	local j = 0
	while current:sub(3, 3) ~= "Z" do
		j = 1 + step % #Instr
		current = Map[current][Instr:sub(j, j)]
		step = step + 1
	end
	return step
end

function gcd( m, n )
    while n ~= 0 do
        local q = m
        m = n
        n = q % n
    end
    return m
end

function lcm( m, n )
    return ( m ~= 0 and n ~= 0 ) and m * n / gcd( m, n ) or 0
end

local result = 1
for node, _ in pairs(Map) do
	if node:sub(3, 3) == "A" then
		result = lcm(result, GetSteps(node))
	end
end
print("Result1", GetSteps("AAA"))
print("Result2", math.floor(result))
