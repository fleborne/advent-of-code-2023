if #arg < 1 then
	print("Filename needed")
	return
end

Result1 = 0
Result2 = 0

Duplicates = {}

Lineno = 0

for l in io.lines(arg[1]) do
	local mine_str = nil
	local winning_str = nil
	local mine = {}
	local winning = {}

	Lineno = Lineno + 1

	winning_str, mine_str = string.match(l, "^Card +%d+: +(.*) | (.*)$")

	for match in string.gmatch(winning_str, " *%d+ *") do
		table.insert(winning, tonumber(match))
	end
	for match in string.gmatch(mine_str, " *%d+ *") do
		table.insert(mine, tonumber(match))
	end

	local wins = 0
	for i = 1, #mine do
		for j = 1, #winning do
			if mine[i] == winning[j] then
				wins = wins + 1
			end
		end
	end
	if wins > 0 then
		Result1 = Result1 + 2^(wins-1)
	end

	for _ = #Duplicates, Lineno+wins do
		table.insert(Duplicates, 1)
	end
	for j = 1, wins do
		Duplicates[Lineno+j] = Duplicates[Lineno+j] + Duplicates[Lineno]
	end
	Result2 = Result2 + Duplicates[Lineno]

end

print("Result1", math.floor(Result1))
print("Result2", Result2)
