function List(...)
	local l = {}
	for v in ... do
		l[#l+1] = v
	end
	return l
end

function BlockToTable(f)
	_ = f()  --discard map name

	local t = {}
	local line = nil

	repeat
		line = f()
		if line ~= nil then
			local dest_start, source_start, length = string.match(line, "(%d+) +(%d+) +(%d+)")
			if length ~= nil then
				t[#t+1] = {
					source_start = tonumber(source_start),
					dest_start = tonumber(dest_start),
					length = tonumber(length)
				}
			end
		end
	until line == "" or line == nil

	return t
end

function FollowMap(map, source)
	for i = 1, #map do
		if map[i].source_start <= source and source <= map[i].source_start + map[i].length then
			return map[i].dest_start + (source - map[i].source_start)
		end

	end
	return source
end

function ProcessSeed(s)
	return FollowMap(HumidityLocation,
		FollowMap(TempHumidity,
		FollowMap(LightTemp,
		FollowMap(WaterLight,
		FollowMap(FertilizerWater,
		FollowMap(SoilFertilizer,
		FollowMap(SeedSoil, s)))))))
end

File = io.lines(arg[1])

Locations = {}

Seeds = List(string.gmatch(File(), "(%d+)"))
_ = File() -- discard empty line

SeedSoil = BlockToTable(File)
SoilFertilizer = BlockToTable(File)
FertilizerWater = BlockToTable(File)
WaterLight = BlockToTable(File)
LightTemp = BlockToTable(File)
TempHumidity = BlockToTable(File)
HumidityLocation = BlockToTable(File)

Result1 = nil
for i = 1, #Seeds do
	local r = ProcessSeed(tonumber(Seeds[i]))

	if Result1 == nil or r < Result1 then
		Result1 = r
	end
end
print("Result1", Result1)


Result2 = nil
function Part2(i)
	local a = Seeds[2*i+1]
	local b = Seeds[2*i+2]
	local gr = nil
	local r = nil
	if a ~= nil and b ~= nil then
		for s = a, a+b do
			r = ProcessSeed(s)
			if gr == nil or r < gr then
				gr = r
			end
		end
	end
	return gr
end
for i = 0, 9 do
	local r = Part2(i)
	if Result2 == nil or r < Result2 then
		Result2 = r
	end
	print(r)
end
print("Result2", Result2)
